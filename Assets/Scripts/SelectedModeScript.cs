﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectedModeScript : MonoBehaviour 
{
	private	static	SelectedModeScript smInstance = null;

	public	static	SelectedModeScript SMInstance
	{
		get { return smInstance; }
	}

	public	string currentMode;

	void	Awake()
	{
		if (smInstance != null && smInstance != this)
		{
			Destroy(gameObject);
		}
		else
		{
			smInstance = this;
		}
		DontDestroyOnLoad(gameObject);
	}

	public	void	SetMode(string mode)
	{
		currentMode = mode;
	}
}
