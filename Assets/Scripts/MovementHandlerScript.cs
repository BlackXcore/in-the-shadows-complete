﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementHandlerScript : MonoBehaviour 
{
	public	bool	ver_rotation;
	public	bool	move;

	public	float	speed = 300;
	public	LayerMask	mask;
	private	GameObject	selectedObj;
	// public	int	level;
	public	int	currentLevel;
	public	int	levelInFile;

	public	string	currentMode;

	void	Start()
	{
		FileManagerScript.CheckAndCreate();
		levelInFile = FileManagerScript.readToVar();
		currentMode = SelectedModeScript.SMInstance.currentMode;
	}
	void Update () 
	{
		if (Input.GetMouseButtonDown(0))
		{
			RaycastHit	hit;
			if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 100, mask))
				selectedObj = hit.transform.gameObject;
			else
				selectedObj = null;
		}
		else if (Input.GetMouseButtonUp(0))
			selectedObj = null;

		if (!InGameManager.InGm.levelEnded)
		{
			if (Input.GetMouseButton(0) && selectedObj != null)
			{
				//MOVEMENT FOR THE SELECT OBJECT
				if (move && Input.GetKey(KeyCode.LeftShift))
					selectedObj.transform.Translate(new Vector3(0, Input.GetAxis("Mouse Y"), 0) * Time.deltaTime * (speed / 295), Space.World);
				else if (ver_rotation && Input.GetMouseButton(1))
					selectedObj.transform.Rotate(new Vector3(Input.GetAxis("Mouse X"), 0, 0) * Time.deltaTime * speed, Space.World);
				else if (ver_rotation && Input.GetKey(KeyCode.LeftControl))
					selectedObj.transform.Rotate(new Vector3(0, 0, Input.GetAxis("Mouse Y")) * Time.deltaTime * speed, Space.World);
				else
					selectedObj.transform.Rotate(new Vector3(0, Input.GetAxis("Mouse X"), 0) * Time.deltaTime * speed, Space.World);
				Vector3 temp = selectedObj.transform.position;
				if (move)
					temp.y = Mathf.Clamp(selectedObj.transform.position.y, selectedObj.GetComponent<ObjectScript>().y_origin_position - 0.2f,  selectedObj.GetComponent<ObjectScript>().y_origin_position + 0.2f);
				selectedObj.transform.position = temp;
			}
		}
		else if (InGameManager.InGm.levelEnded)
		{
			if (currentMode == "Normal")
			{
				if (currentLevel < 5 && (currentLevel + 1) > levelInFile)
				{
					int new_level = currentLevel + 1;
					FileManagerScript.writeToFile("" + new_level);
				}
			}
		}
	}
}
