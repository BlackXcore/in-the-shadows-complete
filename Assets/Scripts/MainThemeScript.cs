﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainThemeScript : MonoBehaviour 
{
	private	static	MainThemeScript instance = null;

	public	static	MainThemeScript Instance
	{
		get { return instance; }
	}

	void Awake()
	{
		//Restricting the gameObject to be instantiated multiple times.
		//Can only be instantiated to one object
		if (instance != null && instance != this)
		{
			Destroy(gameObject.transform);
			return;
		}
		else
		{
			instance = this;
		}
		DontDestroyOnLoad(gameObject);
	}
}
