﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectScript : MonoBehaviour 
{
	public	GameObject	objRef;
	public	bool	correctPos = false;
	public	static	bool	stopMovement = false;
	public	float	precision_angle = 10;
	public	float	y_origin_position;
	void Start () 
	{
		y_origin_position = transform.position.y;
	}
	
	void Update () 
	{
		if (Quaternion.Angle(transform.rotation, objRef.transform.rotation) < precision_angle)
		{
			// Debug.Log("Precision: " + Quaternion.Angle(transform.rotation, objRef.transform.rotation));
			correctPos = true;
		}
		else
		{
			correctPos = false;
		}
	}
}
