﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionsScript : MonoBehaviour 
{
	public	Slider	VolumeSlider;
	public	static	float volumeData = 1f;
	private	static	bool	created = false;
		
	public	void	Start()
	{
		if (AudioListener.volume < 1f)
		{
			VolumeSlider.value = AudioListener.volume;
		}

		VolumeSlider.onValueChanged.AddListener(delegate {ValueChangeCheck(); });
	}

	public	void	ValueChangeCheck()
	{
		AudioListener.volume = VolumeSlider.value;
	}

	public	void	ResetGame()
	{
		FileManagerScript.writeToFile("1");
	}
}
