﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;
using System.Text;

public class FileManagerScript : MonoBehaviour 
{
	private	static	FileManagerScript fmInstance = null;

	public	static FileManagerScript FMInstance
	{
		get { return fmInstance; }
	}
	public	static	string	storage_dir = "./GameContent/";
	public	static	string	path = storage_dir + "game.txt";
	public	static	int	curLevel = 1;

	void	Awake()
	{
		if (fmInstance != null && fmInstance != this)
		{
			Destroy(gameObject);
			return;
		}
		else
		{
			fmInstance = this;
		}
		CheckAndCreate();
		DontDestroyOnLoad(gameObject);
	}

	public	static	void	CheckAndCreate()
	{
		if (!Directory.Exists(storage_dir))
		{
			Directory.CreateDirectory(storage_dir);
		}

		if (!System.IO.File.Exists(path))
		{
			System.IO.File.WriteAllText(path, "1");
		}
		else
		{
			//GET THE DATA FROM THE TEXT FILE
			curLevel = readToVar();
		}
	}

	public static int	readToVar()
	{
		StreamReader	reader = new StreamReader(path);
		string value = reader.ReadToEnd();
		int int_value = Int32.Parse(value);
		reader.Close();

		return	(int_value);
	}

	public	static	void	clearContents()
	{
		FileStream	stream = new FileStream(path, FileMode.Truncate);
		StreamWriter writer = new StreamWriter(stream);
		writer.Write("");
		writer.Close();
		stream.Close();
	}

	public	static	void	writeToFile(string value)
	{
		clearContents();
		StreamWriter writer = new StreamWriter(path, true);
		writer.WriteLine(value);
		writer.Close();
		CheckAndCreate();
	}
}
