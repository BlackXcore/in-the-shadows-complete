﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InLevelMenuScript : MonoBehaviour 
{
	public  void	LoadNormalMenu()
	{
		ObjectScript.stopMovement = false;

		string mode = SelectedModeScript.SMInstance.currentMode;
		
		if (mode == "Normal")
			SceneManager.LoadScene("Scene/Normal Mode");
		else if (mode == "Tester")
			SceneManager.LoadScene("Scene/Test Mode");
	}
}