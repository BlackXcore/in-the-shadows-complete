﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InGameManager : MonoBehaviour
{
    public  GameObject[]    items;
    public  GameObject  successMenu;

    private bool    allObjectsGood = false;
    public  bool    levelEnded = false;
    public  static  InGameManager   InGm;

    void    Start()
    {
        InGm = this;
        items = GameObject.FindGameObjectsWithTag("the_object");
    }

    void    Update()
    {
        foreach (GameObject item in items)
        {
            if (item.GetComponent<ObjectScript>().correctPos)
                allObjectsGood = true;
            else
            {
                allObjectsGood = false;
                break;
            }            
        }

        if (allObjectsGood)
        {
            if (items.Length == 2)
            {
                if (Mathf.Abs (items[0].transform.position.y - items[1].transform.position.y) < 0.01f)
                {
                    Debug.Log("Abs: " + Mathf.Abs (items[0].transform.position.y - items[1].transform.position.y));
                    successMenu.SetActive(true);
                    levelEnded = true;
                }
            }
            else
            {
                successMenu.SetActive(true);
                levelEnded = true;
            }
        }
    }
}
