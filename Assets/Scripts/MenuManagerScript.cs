﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuManagerScript : MonoBehaviour 
{
	public	Slider	volume;

	public	void	ExitButton()
	{
		Debug.Break();
		Application.Quit();
	}

	public	void	NormalMode()
	{
		SelectedModeScript.SMInstance.SetMode("Normal");
		SceneManager.LoadScene("Scene/Normal Mode");
	}

	public	void	TesterMode()
	{
		SelectedModeScript.SMInstance.SetMode("Tester");
		SceneManager.LoadScene("Scene/Test Mode");
	}
}