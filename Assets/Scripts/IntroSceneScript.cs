﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class IntroSceneScript : MonoBehaviour 
{
	public	GameObject	light;
	private	int	counter;
	public	int	end_counter;

	void Start () 
	{
		counter = 0;
		end_counter = 60;

		if (light.activeSelf)
			light.SetActive(false);
	}
	
	void Update () 
	{
		if (counter >= end_counter)
			light.SetActive(true);
		if (counter >= (end_counter + 120))
			SceneManager.LoadScene("Scene/Menu", LoadSceneMode.Single);
		counter++;
	}
}
