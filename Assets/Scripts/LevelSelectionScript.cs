﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelSelectionScript : MonoBehaviour 
{
	private	GameObject	selectedLevel;
	public static int currentLevel = 1;
	public	GameObject[] boxLevels;
	public	GameObject[] lightLevels;
	public	float speed = 250f;
	public	string	currentMode;
	// Update is called once per frame
	void	Start()
	{
		boxLevels = GameObject.FindGameObjectsWithTag("level");
		lightLevels = GameObject.FindGameObjectsWithTag("light");
		currentLevel = FileManagerScript.curLevel;
	}
	void FixedUpdate () 
	{
		currentMode = SelectedModeScript.SMInstance.currentMode;
		animateBox();
		lightBox();

		RaycastHit	hit;
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

		if (Input.GetMouseButtonUp(0))
		{
			if (ray.origin != null)
			{
				if (Physics.Raycast(ray, out hit, 100f))
				{
					ObjectScript.stopMovement = false;
					selectedLevel = hit.transform.gameObject;
				}
				else
				{
					selectedLevel = null;
				}
			}
		}
		else
		{
			selectedLevel = null;
		}

		if (selectedLevel != null)
		{
			if (selectedLevel.name == "Level 1")
				SceneManager.LoadScene("Scene/Levels/Level 1");
			if (selectedLevel.name == "Level 2" && FileManagerScript.curLevel > 1)
				SceneManager.LoadScene("Scene/Levels/Level 2");
			if (selectedLevel.name == "Level 3" && FileManagerScript.curLevel > 2)
				SceneManager.LoadScene("Scene/Levels/Level 3");
			if (selectedLevel.name == "Level 4" && FileManagerScript.curLevel > 3)
				SceneManager.LoadScene("Scene/Levels/Level 4");
			if (selectedLevel.name == "Level 5" && FileManagerScript.curLevel > 4)
				SceneManager.LoadScene("Scene/Levels/Level 5");
			else if (currentMode == "Tester")
			{
				if (selectedLevel.name == "Level 1")
					SceneManager.LoadScene("Scene/Levels/Level 1");
				if (selectedLevel.name == "Level 2")
					SceneManager.LoadScene("Scene/Levels/Level 2");
				if (selectedLevel.name == "Level 3")
					SceneManager.LoadScene("Scene/Levels/Level 3");
				if (selectedLevel.name == "Level 4")
					SceneManager.LoadScene("Scene/Levels/Level 4");
				if (selectedLevel.name == "Level 5")
					SceneManager.LoadScene("Scene/Levels/Level 5");
			}
		}
	}

	public	void	animateBox()
	{
		if (boxLevels != null)
		{
			foreach (GameObject currentBox in boxLevels)
			{
				if (currentMode == "Normal")
				{
					if (currentLevel == 1 && currentBox.name == "Level 1")
						currentBox.transform.Rotate(Vector3.up, speed * Time.deltaTime);
					if (currentLevel == 2 && currentBox.name == "Level 2")
						currentBox.transform.Rotate(Vector3.up, speed * Time.deltaTime);
					if (currentLevel == 3 && currentBox.name == "Level 3")
						currentBox.transform.Rotate(Vector3.up, speed * Time.deltaTime);
					if (currentLevel == 4 && currentBox.name == "Level 4")
						currentBox.transform.Rotate(Vector3.up, speed * Time.deltaTime);
					if (currentLevel == 5 && currentBox.name == "Level 5")
						currentBox.transform.Rotate(Vector3.up, speed * Time.deltaTime);
				}
				else if (currentMode == "Tester")
				{
					currentBox.transform.Rotate(Vector3.up, speed * Time.deltaTime);
				}
			}
		}
	}

	public	void	lightBox()
	{
		if (lightLevels != null)
		{
			foreach (GameObject	currentLight in lightLevels)
			{
				if (currentMode == "Normal")
				{
					if (currentLevel > 0 && currentLight.name == "Light 1")
						currentLight.SetActive(false);
					if (currentLevel > 1 && currentLight.name == "Light 2")
						currentLight.SetActive(false);
					if (currentLevel > 2 && currentLight.name == "Light 3")
						currentLight.SetActive(false);
					if (currentLevel > 3 && currentLight.name == "Light 4")
						currentLight.SetActive(false);
					if (currentLevel > 4 && currentLight.name == "Light 5")
						currentLight.SetActive(false);
				}
				else if (currentMode == "Tester")
				{
					currentLight.SetActive(false);					
				}
			}
		}
	}
}
