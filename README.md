# In The Shadows

In the shadows is the first unity project at WeThinkCode_ from the UNITY MODULE.
This is a replica of the Shadowmatic game. https://www.shadowmatic.com/

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

What you need to run this Unity3D https://unity3d.com/

### Installing

To install you'll have to clone the project to your local machine

Step 1: Clone the project
```
git clone https://gitlab.com/BlackXcore/in-the-shadows-complete
```
Step 2: Open the project in Unity
```
Open unity and click open and browse to the folder of the project and open it.
```

Step 3: Build and Run the project
```
Go to edit on the top bar and click edit and click build and run.
```

### Testing using the normal mode or the tester mode

After click the start on the Main Menu you are required to choose between the normal mode or the test mode.


### Normal Mode

In normal mode you have to complete the current level before going to the next one.

### Tester Mode

In tester you can choose any level you like. You don't need to complete the current one to go to the next level.

## Built With

* [Visual Studio Code](https://code.visualstudio.com/) - Visual Studio Code is a source code editor developed by Microsoft for Windows, Linux and macOS.
* [Unity3D](https://unity3d.com/) - Unity is a cross-platform game engine developed by Unity Technologies

## Authors

* **Afrika Matshiye** - [Github](https://github.com/amatshiye)

## Acknowledgments

* **Brackeys** - [YouTube](https://www.youtube.com/user/Brackeys)

* **Hardly Difficult** - [YouTube](https://www.youtube.com/channel/UC3bHnBF2Q-u-1NEYG0Xwgeg)